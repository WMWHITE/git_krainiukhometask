package Objects;

public class Cars {
    public String trademark;
    public String model;
    public String engine;
    public double engineVolume;
    private int year; /*year of manufacture*/
    public boolean isNew; /*new or used*/

    public Cars (String trademark, String model, String engine, double engineVolume, int year, boolean isNew) {
        this.trademark = trademark;
        this.model = model;
        this.engine = engine;
        this.engineVolume = engineVolume;
        this.year = year;
        this.isNew = isNew;
    }



    public String stopCar(int liter) {
        String str;
               if (liter <= 1) str ="Остановить авто!";
        else if (liter<=5) str = "У Вас заканчивается бензин, проследуйте на заправку";
        else str ="Продолжай движение";
                return (str);
             }
             public String limitSpeed(int speed)
             {
                 String str1;
                 if (speed <=100) str1 = "У тебя средняя скорость";
                 else if (speed <=120) str1 = "Ты уже гонишь!";
                 else if (speed <=140) str1 = "Превышен лимит скорости";
                 else str1 = "Срочно сбрось скорость";
                 return (str1);
             }
             public double kmToMile(double distance, boolean isKM) {
                   if (isKM) distance *= 0.621371;
                     else distance *= 1.60934;
                        return (distance);
                 }
    public int getYear () {
                return this.year;
    }


    }




