package Objects;

public class Lorry extends Cars{
    public int power;
    public int payload;

    public Lorry (String trademark, String model, String engine, double engineVolume, int year, boolean isNew, int power, int payload) {
        super (trademark, model, engine, engineVolume,year,isNew);
        this.power = power;
        this.payload = payload;
    }
        @Override
        public String limitSpeed( int speed)
        {
            String str1;
            if (speed <=80) str1 = "У тебя средняя скорость";
            else if (speed <=90) str1 = "Ты уже гонишь!";
            else if (speed <=120) str1 = "Превышен лимит скорости";
            else str1 = "Срочно сбрось скорость";
            return (str1);
        }

    @Override
    public String toString() {
        return "Lorry{" +
                "power=" + power +
                ", payload=" + payload +
                ", trademark='" + trademark + '\'' +
                ", model='" + model + '\'' +
                ", engine='" + engine + '\'' +
                ", engineVolume=" + engineVolume +
                ", isNew=" + isNew +
                '}';
    }
}





