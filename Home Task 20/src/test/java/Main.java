import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.internal.MouseAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;

public class Main {
 WebDriver driver;

    @BeforeClass
    public static void mainPrecondition() {
        System.setProperty ("webdriver.gecko.driver", "C:\\driver for QA Maven - Idea\\geckodriver-v0.26.0-win64\\geckodriver.exe");
    }

    @Before
    public void precondition() {
        driver = new FirefoxDriver ();
         driver.get ("https://user-data.hillel.it/html/registration.html");
    }

    @Test
    public void registration() throws InterruptedException {
        driver.findElement (By.className ("registration")).sendKeys ("display: block",Keys.ENTER);
        Thread.sleep(3000);
        driver.findElement(By.id ("first_name")).sendKeys ("Andrii");
        driver.findElement(By.id ("last_name")).sendKeys ("Krainiuk");
        driver.findElement(By.id ("field_work_phone")).sendKeys ("7485012");
        driver.findElement(By.id ("field_phone")).sendKeys ("380501110115");
        driver.findElement(By.id ("field_email")).sendKeys ("andrii.krainiuk@gmail.com");
        driver.findElement(By.id ("field_password")).sendKeys ("Andrii169");
        driver.findElement(By.id ("male")).click ();
        driver.findElement (By.xpath ("//select/option[3]")).click ();;
        /*driver.findElement(By.id ("position")).sendKeys ("qa",Keys.ENTER); - работаете и так*/
        driver.findElement(By.id ("button_account")).click ();
        Thread.sleep(3000);

        WebDriverWait wait = new WebDriverWait(driver, 5);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        alert.accept();

        driver.navigate ().refresh ();
        Thread.sleep(1000);
        driver.findElement(By.id ("email")).sendKeys ("andrii.krainiuk@gmail.com",Keys.ENTER);
        driver.findElement(By.id ("password")).sendKeys ("Andrii169",Keys.ENTER);
        driver.findElement(By.className ("login_button")).click ();

    }

    @After
    public void after() {
        driver.quit ();
    }
    @AfterClass
    public static void afterclass() {
        System.out.println ("Ура, ты это сделал!");
    }

}
